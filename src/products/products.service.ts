import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';


let products: Product[] = [
  { id: 1, name: "chocolate", price: 80 },
  { id: 2, name: "almond milk", price: 75 },
];

let lastProductId = 3;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto
    }
    products.push(newProduct)
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    })
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    })
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateUser: Product = {
      ...products[index],
      ...updateProductDto
    }
    products[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    })
    if (index < 0) {
      throw new NotFoundException();
    }

    const deleteProduct = products[index]
    products.splice(index, 1)
    return deleteProduct;
  }

  reset(){
    products = [
      { id: 1, name: "chocolate", price: 80 },
      { id: 2, name: "almond milk", price: 75 },
    ],
    lastProductId = 3;
    return 'RESEST';
  }
}
